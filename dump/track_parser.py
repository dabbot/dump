import struct
from typing import Tuple

class Track:
    author: str
    has_url: bool
    identifier: str
    length: int
    source: str
    stream: bool
    title: str
    url: str
    version: int

    def __init__(
        self,
        author: str,
        has_url: bool,
        identifier: str,
        length: int,
        source: str,
        stream: bool,
        title: str,
        url: str,
        version: int,
    ):
        self.author = author
        self.has_url = has_url
        self.identifier = identifier
        self.length = length
        self.source = source
        self.title = title
        self.url = url
        self.version = version

def _read_size(idx, track) -> Tuple[int, int]:
    (size,) = struct.unpack('>H', track[idx:idx + 2])

    return [idx + 2, size]

def _read_8bit_int(idx, track) -> Tuple[int, int]:
    (value,) = struct.unpack('B', track[idx:idx + 1])

    return [idx + 1, value]

def _read_string(idx, track) -> Tuple[int, str]:
    [idx, size] = _read_size(idx, track)

    try:
        value = track[idx:idx + size].decode('utf-8')
    except UnicodeDecodeError:
        value = track[idx:idx + size].decode('latin-1')

    return [idx + size, value]

def _read_64bit_int(idx, track) -> Tuple[int, int]:
    (value,) = struct.unpack('>Q', track[idx: idx + 8])

    return [idx + 8, value]


def parse_track(track: bytes) -> Track:
    idx = 0
    (value,) = struct.unpack('B', track[idx:idx + 1])
    idx += 1

    flags = (value & 0xC000_0000) >> 30

    # message size, we ignore
    idx += 2

    if flags & 1 == 0:
        version = 1
    else:
        version = struct.unpack('B', track[idx:idx + 1])
        idx += 1

    # don't care about next 2 bytes
    idx += 2

    [idx, title] = _read_string(idx, track)
    [idx, author] = _read_string(idx, track)
    [idx, length] = _read_64bit_int(idx, track)
    [idx, identifier] = _read_string(idx, track)
    [idx, stream] = _read_8bit_int(idx, track)
    stream = bool(stream)
    [idx, has_url] = _read_8bit_int(idx, track)
    has_url = bool(has_url)

    url = None

    if has_url:
        [idx, url] = _read_string(idx, track)
    else:
        idx += 2

    [_, source] = _read_string(idx, track)

    return Track(
        author=author,
        has_url=has_url,
        identifier=identifier,
        length=length,
        source=source,
        stream=stream,
        title=title,
        url=url,
        version=version,
    )
