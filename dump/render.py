import base64
import html
import json
import math

from .track_parser import parse_track

def render(uuid: str, content: bytes) -> str:
    tracks_b64 = json.loads(content)

    playlist_seconds = 0
    playlist_buf = ''

    for (idx, track_b64) in enumerate(tracks_b64):
        try:
            track = parse_track(base64.b64decode(track_b64))
        except UnicodeDecodeError as e:
            print(e)

            playlist_buf += '<li>Error parsing track data</li>'

            continue

        playlist_seconds += track.length // 1000

        seconds = math.floor(track.length / 1000)
        author = html.escape(track.author)
        title = html.escape(track.title)
        playlist_buf += f'<li><em>{title}</em> <strong>by</strong> '
        playlist_buf += f'<em>{author}</em>'
        playlist_buf += f'<br>Length: {seconds} seconds'

        if track.has_url:
            playlist_buf += f'<br>URL: {track.source} ('
            playlist_buf += f'<a href="{track.url}" target="_blank">{track.url}</a>)'

        playlist_buf += '<br><br></li>'

    playlist_length = math.floor(playlist_seconds / 60)
    buf = '<!doctype html><html><body><h4>Songs in your playlist</h4>'
    buf += f'<p>Total tracks: <strong>{len(tracks_b64)}</strong></p>'
    buf += f'<p>Total playlist length: <strong>{playlist_length} minutes</strong></p>'
    buf += f'<br><p>To load this playlist, use:<br><br>'
    buf += f'<strong>!!!load {uuid}</strong></p>'
    buf += '<ol>'
    buf += playlist_buf
    buf += '</ol></body></html>'

    return buf
