
import json
from json import JSONDecodeError
import uuid
from uuid import UUID

import aiofiles
from aiohttp import web
from aiohttp.web import Application, Request, Response

from .render import render

class Dump:
    _app: Application
    _authorization: str
    _host: str
    _port: int

    def __init__(
        self,
        authorization: str,
        *,
        host: str='0.0.0.0',
        port: int=15050,
    ):
        self._authorization = authorization
        self._host = host
        self._port = port

        self._app = Application()
        self._app.add_routes([
            web.get('/', self._index),
            web.post('/', self._dump),
            web.get('/{dump_id}', self._retrieve),
        ])

    @property
    def host(self) -> str:
        return self._host

    @property
    def port(self) -> int:
        return self._port

    def run(self) -> None:
        web.run_app(self._app, host=self.host, port=self.port)

    @staticmethod
    def _index(_: Request) -> Response:
        return Response(status=200, text='Hello!')

    async def _dump(self, req: Request) -> Response:
        try:
            auth = req.headers['Authorization']
        except KeyError:
            return Response(text='No authorization.', status=401)

        if auth != self._authorization:
            return Response(text='Bad authorization.', status=401)

        body = await req.text()

        if len(body) < 2:
            return Response(status=400, text='Not large enough.')

        new_uuid = str(uuid.uuid4())

        async with aiofiles.open(f'./storage/{new_uuid}', mode='w') as f:
            await f.write(body)

        return json_response(201, json.dumps({
            'uuid': new_uuid,
        }))

    async def _retrieve(self, req: Request) -> Response:
        dump_id = req.match_info['dump_id']

        if not validate_uuid4(dump_id):
            return Response(status=400, text='Invalid ID.')

        webview = True

        try:
            webview = req.headers['User-Agent'] != 'dabbot'
        except KeyError:
            pass

        async with aiofiles.open(f'./storage/{dump_id}', mode='r') as f:
            content = await f.read()

        if webview:
            body = render(dump_id, content)

            return text_response(200, body)
        else:
            return json_response(200, content)


def json_response(status: int, body: str) -> Response:
    return Response(
        status=status,
        text=body,
        content_type='application/json',
    )


def text_response(status: int, body: str) -> Response:
    return Response(
        status=status,
        text=body,
        content_type='text/html',
    )


def validate_uuid4(uuid_input: str) -> bool:
    try:
        uuid = UUID(uuid_input, version=4)
    except ValueError:
        return False

    return uuid.hex == uuid_input.replace('-', '')
