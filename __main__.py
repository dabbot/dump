import os
from os.path import join, dirname

from dotenv import load_dotenv

from dump import Dump

if __name__ == '__main__':
    dotenv_path = join(dirname(__file__), '.env')
    load_dotenv(dotenv_path)

    auth = os.environ['AUTHORIZATION']
    host = os.environ.get('APP_HOST', '0.0.0.0')
    port = int(os.environ.get('APP_PORT', 80))

    Dump(auth, host=host, port=port).run()
