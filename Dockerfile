FROM frolvlad/alpine-python3

ENV AUTHORIZATION hi

RUN USER=root mkdir /app
WORKDIR /app

COPY ./dump ./dump
COPY ./requirements.txt ./requirements.txt
COPY ./__main__.py ./__main__.py

RUN pip3 install -r requirements.txt

RUN mkdir ./storage

CMD python3 /app
